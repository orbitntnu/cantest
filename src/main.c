/*
 * Copyright (C) 2018 Erlend Sveen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <stdio.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

struct can_frame
{
	unsigned int can_id;
	unsigned char can_dlc;
	char padding1;
	char padding2;
	char padding3;
	unsigned char data[8];
};

int sendmsg (int fd, char *msg, int len)
{
	struct can_frame txf;
	txf.can_id = 0x00005555;
	txf.can_dlc = len;
	txf.data[0] = 'a';
	txf.data[1] = 'b';
	txf.data[2] = 'c';
	txf.data[3] = 'd';
	txf.data[4] = 'e';
	txf.data[5] = 'f';
	txf.data[6] = 'g';
	txf.data[7] = 'h';

	for (int i = 0; i < len && i < 8; i++)
		txf.data[i] = msg[i];

	return write (fd, &txf, sizeof (txf));
}

int main (int argc, char **argv)
{
	struct can_frame rxf;

	int fd = open ("/dev/can2", O_RDWR);

	if (fd < 0)
	{
		printf ("Failed to open fd errno %i\n", errno);
		return fd;
	}

	while (1)
	{
		char outdata[8];
		int n = 0;

		for (; n < 8; n++)
			if (read (0, &outdata[n], 1) != 1)
				break;

		if (n > 0)
			if (sendmsg (fd, outdata, n) < 0)
				printf ("Failed to send crap\n");

		int retr = read (fd, &rxf, sizeof (rxf));

		if (retr > 0)
		{
			char str[9];
			str[0] = rxf.data[0];
			str[1] = rxf.data[1];
			str[2] = rxf.data[2];
			str[3] = rxf.data[3];
			str[4] = rxf.data[4];
			str[5] = rxf.data[5];
			str[6] = rxf.data[6];
			str[7] = rxf.data[7];
			str[8] = 0;

			if (rxf.can_dlc >= 0 && rxf.can_dlc < 8)
				str[rxf.can_dlc] = 0;

			if (str[0] == '\r')
				str[0] = '\n';

			printf ("%s", str);
		}
		else if (retr < 0)
		{
			printf ("Read failed with errno %i\n", retr);
		}
	}

	return 0;
}
