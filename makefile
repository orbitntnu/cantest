###############################################################################
# Project name and files
###############################################################################
PROJECT_NAME = cantest
PROJECT_FILES = $(shell find src -name "*.*")

NOREL_BASE_TEXT   = 0x20010000
NOREL_BASE_RODATA = 0x20013000
NOREL_BASE_DATA   = 0x20014000
NOREL_BASE_BSS    = 0x20014434

LIB = ../../lib/

USR_INC = -I$(LIB)libuser/inc
USR_LIB = -L$(LIB)libuser/build
USR_LIBS = -lcglue -luser

###############################################################################
# Compiler flags, file processing and makefile execution
###############################################################################
include ../../util/build/makefile/flags
include ../../util/build/makefile/processfiles
include ../../util/build/makefile/targets
